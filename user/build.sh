#!/bin/bash -e

REGISTRY="registry.gitlab.com"

build_image(){
    docker build --no-cache -t ${CI_REGISTRY_IMAGE}/user:latest -f ./user/Dockerfile.build ./user
    docker push ${CI_REGISTRY_IMAGE}/user:latest
}

build_image