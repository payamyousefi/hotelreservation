import controller from '../controller'
import Router from 'express-promise-router'
const router = Router()

router.post('/v1/user', controller.addUserCtrl)
router.put('/v1/user/point', controller.changeUserPointCtrl)
router.get('/v1/user/:id', controller.getUserByIdCtrl)
router.get('/v1/user', controller.searchUserCtrl)

export default router
