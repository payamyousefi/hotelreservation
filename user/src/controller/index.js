import { addUser, searchUser, changeUserPoint, getUserById } from '../use-cases'
import createError from 'http-errors'

const addUserCtrl = async (request, response) => {
  const { family, email, name } = request.body
  if (!email) {
    throw createError.BadRequest('valid email is required')
  }
  if (!name) {
    throw createError.BadRequest('name is required')
  }
  if (!family) {
    throw createError.BadRequest('family is required')
  }

  const result = await addUser({ email, name, family })
  response.send(result)
}

const getUserByIdCtrl = async (request, response) => {
  console.log(request.params)
  console.log(request.param)
  const userId = request.params.id
  if (!userId) {
    throw createError.BadRequest('userId is required')
  }
  const user = await getUserById(userId)
  console.log(user)
  response.send(user)
}

const searchUserCtrl = async (request, response) => {
  const filter = request.query
  const users = await searchUser(filter)
  response.send(users)
}

const changeUserPointCtrl = async (request, response) => {
  const points = request.body.points
  const userId = request.body.userId
  if (!userId) {
    throw createError.BadRequest('userId is required')
  }
  if (points === undefined) {
    throw createError.BadRequest('points is required')
  }
  const user = await changeUserPoint(userId, points)
  response.send(user)
}

export default {
  addUserCtrl,
  getUserByIdCtrl,
  searchUserCtrl,
  changeUserPointCtrl
}
