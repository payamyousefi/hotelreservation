import { createMockDb, closeMockDbConnection } from './db-connection-mock'
import userDbFactory from './user-db-factory'
import { ObjectId } from 'mongodb'

describe('insert new user', () => {
  let userDb
  beforeAll(async () => {
    userDb = userDbFactory({ getDbConnection: createMockDb })
  })

  afterAll(async () => {
    await closeMockDbConnection()
  })

  it('should insert a doc into mongo collection', async () => {
    const mockUser = { _id: new ObjectId(), name: 'ali', age: 12 }
    const createdUser = await userDb.insert(mockUser)
    expect(createdUser._id).toEqual(mockUser._id)
    expect(createdUser).toEqual(mockUser)
  })

  it('should remove a doc from mongo collection', async () => {
    const mockUser = { _id: new ObjectId(), name: 'payam', age: 35 }
    await userDb.insert(mockUser)
    const result = await userDb.delete(mockUser._id)

    expect(result.ok).toBe(true)
    expect(result.removed).toBe(1)
  })

  it('should update a doc in mongo collection', async () => {
    const mockUser = { _id: new ObjectId(), name: 'payam', age: 20 }
    await userDb.insert(mockUser)
    const updatedUser = await userDb.update(mockUser._id, { name: 'ali' })
    expect(updatedUser.name).toEqual('ali')
    expect(updatedUser.age).toEqual(20)
  })

  it('should find user by id', async () => {
    const mockUser = { _id: new ObjectId(), name: 'payam', age: 20 }
    await userDb.insert(mockUser)
    const selectedUser = await userDb.findById(mockUser._id)
    expect(selectedUser._id).toEqual(mockUser._id)
  })

  it('should find users by name', async () => {
    const mockUser = { _id: new ObjectId(), name: 'amir', family: 'yousefi' }
    await userDb.insert(mockUser)
    await userDb.insert({ _id: new ObjectId(), name: 'hamideh', family: 'yousefi' })
    const users = await userDb.find({ name: mockUser.name, family: mockUser.family })
    expect(users).not.toEqual(undefined)
    expect(users.length).toBe(1)
    for (const user of users) {
      expect(user.name).toEqual(mockUser.name)
      expect(user.family).toEqual(mockUser.family)
    }
  })
})
