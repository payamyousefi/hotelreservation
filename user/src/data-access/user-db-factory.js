import { ObjectId } from 'mongodb'

export default (dependencies) => {
  const { getDbConnection } = dependencies
  const COLLECTION_NAME = 'users'
  if (!getDbConnection) {
    throw new Error('dependency injection failed')
  }

  return Object.freeze({

    // insert a user-data into mongodb
    insert: async (userData) => {
      const db = await getDbConnection()
      const collection = db.collection(COLLECTION_NAME)
      const result = await collection.insertOne(userData)
      return collection.findOne({ _id: ObjectId(result.insertedId) })
    },

    // find user from mongodb by id
    findById: async (_id) => {
      const db = await getDbConnection()
      const collection = db.collection(COLLECTION_NAME)
      return collection.findOne({ _id: ObjectId(_id) })
    },

    // find user filter
    find: async (filter = {}) => {
      const db = await getDbConnection()
      const collection = db.collection(COLLECTION_NAME)
      const query = {}
      for (const key in filter) {
        /* eslint-disable-next-line */
        if (filter.hasOwnProperty(key) && filter[key]) {
          if (key === '_id') {
            query[key] = ObjectId(filter[key])
          } else {
            query[key] = filter[key]
          }
        }
      }
      const cursor = collection.find(query)
      return cursor.toArray()
    },

    // delete user from mongodb
    delete: async (_id) => {
      const db = await getDbConnection()
      const collection = db.collection(COLLECTION_NAME)
      const response = await collection.deleteOne({ _id: ObjectId(_id) }, { justOne: true })
      // return number of removed objects
      return { removed: response.result.n, ok: response.result.ok === 1 }
    },

    // update user attributes
    update: async (_id, updates) => {
      const db = await getDbConnection()
      const collection = db.collection(COLLECTION_NAME)
      await collection.updateOne({ _id: ObjectId(_id) }, { $set: { ...updates } })
      return collection.findOne({ _id: ObjectId(_id) })
    }

  })
}
