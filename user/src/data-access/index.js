import userDbFactory from './user-db-factory'
import { getDbConnection } from '../utils/mongo-connection'

const userDb = userDbFactory({ getDbConnection })
// Create userDb instance with dependencies
export {
  userDb
}
