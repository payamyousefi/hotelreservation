import amqp from 'amqplib'
import brokerFactory from './broker-factory'

const getRabbitMqChannel = async (exchange) => {
  const connection = await amqp.connect(
        `amqp://${process.env.RABBITMQ_USERNAME}:${
        process.env.RABBITMQ_PASSWORD
        }@rabbitmq`
  )
  const channel = await connection.createChannel()
  channel.assertExchange(exchange, 'fanout', {
    durable: false
  })
  return channel
}

const broker = brokerFactory({ getBroker: getRabbitMqChannel, exchange: 'trivagoEvents' })

export {
  broker
}
