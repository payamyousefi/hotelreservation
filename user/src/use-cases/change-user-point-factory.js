import user from '../entities/user'
import { createEvent } from '../entities/event'

export default (dependencies) => {
  const { userDb, broker } = dependencies
  if (!userDb || !broker) {
    throw new Error('dependency injection failed')
  }
  return async (_id, points) => {
    const userInfo = await userDb.findById(_id)
    if (!userInfo) {
      throw new Error('user not found')
    }
    const userObj = user(userInfo)
    userObj.addPoints(points)
    const updateResult = await userDb.update(_id, { points: userObj.getPoints() })

    const event = createEvent({ verb: 'change-user-point', meta: updateResult })
    await broker.publish(event)
    return updateResult
  }
}
