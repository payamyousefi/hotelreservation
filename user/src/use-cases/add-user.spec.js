import addUserFactory from './add-user-factory'
import { createMockDb, closeMockDbConnection } from '../data-access/db-connection-mock'
import userDbFactory from '../data-access/user-db-factory'

describe('test add user use-case', () => {
  let userDb
  let addUser

  beforeAll(async () => {
    userDb = userDbFactory({ getDbConnection: createMockDb })
    addUser = addUserFactory({ userDb })
  })

  afterAll(async () => {
    await closeMockDbConnection()
  })

  it('should create new user', async () => {
    const userInfo = {
      name: 'payam',
      family: 'yousefi',
      email: 'root.p.y@gmail.com'
    }
    const createdUser = await addUser(userInfo)
    expect(createdUser).toMatchObject({
      name: userInfo.name,
      family: userInfo.family,
      username: userInfo.email,
      email: userInfo.email,
      points: 0
    })
  })
})
