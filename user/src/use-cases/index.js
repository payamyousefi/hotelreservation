import addUserFactory from './add-user-factory'
import changeUserPointFactory from './change-user-point-factory'
import getUserByIdFactory from './get-user-by-id-factory'
import searchUserFactory from './search-user-factory'
import { userDb } from '../data-access'
import { broker } from '../message-broker'

const addUser = addUserFactory({ userDb })
const changeUserPoint = changeUserPointFactory({ userDb, broker })
const getUserById = getUserByIdFactory({ userDb })
const searchUser = searchUserFactory({ userDb })
export { addUser, changeUserPoint, getUserById, searchUser }
