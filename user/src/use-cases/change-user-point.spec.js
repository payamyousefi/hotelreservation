import addUserPointFactory from './change-user-point-factory'
import addUserFactory from './add-user-factory'

import { createMockDb, closeMockDbConnection } from '../data-access/db-connection-mock'
import userDbFactory from '../data-access/user-db-factory'

describe('test add point use-case', () => {
  let userDb
  let addUser

  beforeAll(async () => {
    userDb = userDbFactory({ getDbConnection: createMockDb })
    addUser = addUserFactory({ userDb })
  })

  afterAll(async () => {
    await closeMockDbConnection()
  })

  it('should add point to user', async () => {
    const addUserPoint = addUserPointFactory({
      userDb,
      broker: {
        publish: jest.fn(() => {})
      }
    })
    const userInfo = {
      name: 'payam',
      family: 'yousefi',
      email: 'root@gmail.com'
    }

    const createdUser = await addUser(userInfo)
    const updatedUser = await addUserPoint(createdUser._id, 10)
    expect(updatedUser.points).toBe(10)
    const secondTimeUpdate = await addUserPoint(createdUser._id, 15)
    expect(secondTimeUpdate.points).toBe(25)
  })
})
