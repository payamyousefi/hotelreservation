import user from '../entities/user'

export default dependencies => {
  const { userDb } = dependencies
  if (!userDb) {
    throw new Error('dependency injection failed')
  }
  return async userInfo => {
    const userObj = user(userInfo)
    return userDb.insert({
      _id: userObj.getId(),
      name: userObj.getName(),
      family: userObj.getFamily(),
      username: userObj.getUsername(),
      email: userObj.getUsername(),
      points: userObj.getPoints()
    })
  }
}
