export default (dependencies) => {
  const { userDb } = dependencies
  if (!userDb) {
    throw new Error('dependency injection failed')
  }
  return async filter => {
    const { name, family, email } = filter
    return userDb.find({ name, family, email })
  }
}
