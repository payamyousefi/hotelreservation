import { createMockDb, closeMockDbConnection } from '../data-access/db-connection-mock'
import findUserFactory from './search-user-factory'
import userDbFactory from '../data-access/user-db-factory'

describe('test find user ', () => {
  let userDb

  beforeAll(async () => {
    userDb = userDbFactory({ getDbConnection: createMockDb })
  })

  afterAll(async () => {
    await closeMockDbConnection()
  })

  it('should find user by filter', async () => {
    const userInfo = {
      name: 'payam',
      family: 'yousefi',
      email: 'foundedroot.p.y@gmail.com'
    }
    await userDb.insert(userInfo)
    await userDb.insert({
      name: 'ali',
      family: 'yousefi',
      email: 'sedondedroot.p.y@gmail.com'
    })
    const findUser = await findUserFactory({ userDb })

    const users = await findUser({
      name: userInfo.name,
      family: userInfo.family,
      email: userInfo.email
    })
    expect(users.length).toBe(1)
  })
})
