export default (dependencies) => {
  const { userDb } = dependencies
  if (!userDb) {
    throw new Error('dependency injection failed')
  }
  return async (userId) => {
    return userDb.findById(userId)
  }
}
