import { createMockDb, closeMockDbConnection } from '../data-access/db-connection-mock'
import findUserByIdFactory from './get-user-by-id-factory'
import userDbFactory from '../data-access/user-db-factory'

describe('test find user by id', () => {
  let userDb

  beforeAll(async () => {
    userDb = userDbFactory({ getDbConnection: createMockDb })
  })

  afterAll(async () => {
    await closeMockDbConnection()
  })
  it('should find user by id', async () => {
    const userInfo = {
      name: 'payam',
      family: 'yousefi',
      email: 'root.p.y@gmail.com'
    }
    const createdUser = await userDb.insert(userInfo)
    const findUserById = await findUserByIdFactory({ userDb })
    const foundUser = await findUserById(createdUser._id)
    expect(foundUser._id).toEqual(createdUser._id)
  })
})
