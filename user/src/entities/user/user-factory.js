export default dependencies => {
  const { emailValidator } = dependencies
  if(!emailValidator){
    throw new Error('dependency injection failed')
  }
  return userInfo => {
    if (!userInfo) {
      throw new Error('user info is required')
    }
    const { email, name, family, _id } = userInfo
    let { points = 0 } = userInfo
    const roles = []

    if (!email) {
      throw new Error('email is required')
    }

    if (!emailValidator(email)) {
      throw new Error('invalid email address')
    }

    if (!name) {
      throw new Error('name is required')
    }

    if (!family) {
      throw new Error('family is required')
    }

    return Object.freeze({
      getUsername: () => email,
      getName: () => name,
      getFamily: () => family,
      addRole: role => roles.push(role),
      addPoints: (additionalPoints) => (points += additionalPoints),
      getRoles: () => roles,
      getId: () => _id,
      getPoints: () => points
    })
  }
}
