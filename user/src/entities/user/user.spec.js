import user from './'

describe('test user entity', () => {
  it('should throw error on empty user info', () => {
    expect(() => user()).toThrowError('user info is required')
  })

  it('should throw error on empty email', () => {
    expect(() => user({})).toThrowError('email is required')
  })

  it('should throw error on invalid email', () => {
    expect(() => user({ email: 'payam.com' })).toThrowError('invalid email address')
  })

  it('should throw error on empty name', () => {
    const userInfo = {
      email: 'root.p.y@gmail.com'
    }
    expect(() => user(userInfo)).toThrowError('name is required')
  })

  it('should throw error on empty family', () => {
    const userInfo = {
      email: 'root.p.y@gmail.com',
      name: 'payam'
    }
    expect(() => user(userInfo)).toThrowError('family is required')
  })

  it('should create user object', () => {
    const userInfo = {
      email: 'root.p.y@gmail.com',
      name: 'payam',
      family: 'yousefi'
    }
    const userObj = user(userInfo)
    expect(userObj.getName()).toEqual(userInfo.name)
    expect(userObj.getFamily()).toBe(userInfo.family)
    expect(userObj.getUsername()).toEqual(userInfo.email)
  })

  it('should add role to user', () => {
    const userInfo = {
      email: 'root.p.y@gmail.com',
      name: 'payam',
      family: 'yousefi'
    }

    const userObj = user(userInfo)
    userObj.addRole('customer')
    expect(userObj.getRoles()).toEqual(['customer'])
  })

  it('should add point to user', () => {
    const userInfo = {
      email: 'root.p.y@gmail.com',
      name: 'payam',
      family: 'yousefi',
      points: 12
    }

    const userObj = user(userInfo)
    userObj.addPoints(3)
    expect(userObj.getPoints()).toBe(15)
  })
})
