import user from './'
import uuid from 'uuid'

const createMockUser = (info) => {
  return user({
    _id: uuid.v4(),
    ...info
  })
}

export {
  createMockUser
}
