import userFactory from './user-factory'

const emailValidator = (emailAddress) => {
  /* eslint-disable-next-line */
  var emailRegEx = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return emailRegEx.test(String(emailAddress).toLowerCase())
}

const user = userFactory({
  emailValidator
})

export default user
