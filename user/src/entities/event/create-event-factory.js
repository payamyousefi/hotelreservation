export default () => {
  
  return (messageInfo) => {
    const {verb,meta}=  messageInfo
    if(!verb || !meta){
      throw new Error('invalid message info');
    }
    const producer="user-service"
    
    return {
      producer,
      verb,
      meta,
      creationDate:new Date()
    }
  }
}
