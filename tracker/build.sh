#!/bin/bash -e

REGISTRY="registry.gitlab.com"

build_image(){
    docker build --no-cache -t ${CI_REGISTRY_IMAGE}/tracker:latest -f ./tracker/Dockerfile.build ./tracker
    docker push ${CI_REGISTRY_IMAGE}/tracker:latest
}

build_image