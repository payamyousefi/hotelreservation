import { ObjectId } from 'mongodb'

export default (dependencies) => {
  const { getDbConnection } = dependencies
  const COLLECTION_NAME = 'events'
  if (!getDbConnection) {
    throw new Error('dependency injection failed')
  }

  return Object.freeze({

    // insert a event-data into mongodb
    insert: async (eventData) => {
      const db = await getDbConnection()
      const collection = db.collection(COLLECTION_NAME)
      const result = await collection.insertOne(eventData)
      return collection.findOne({ _id: ObjectId(result.insertedId) })
    },

    // find event from mongodb by id
    findById: async (_id) => {
      const db = await getDbConnection()
      const collection = db.collection(COLLECTION_NAME)
      return collection.findOne({ _id: ObjectId(_id) })
    },

    // find event filter
    find: async (filter = {}) => {
      const db = await getDbConnection()
      const collection = db.collection(COLLECTION_NAME)
      const query = {}
      for (const key in filter) {
        /* eslint-disable-next-line */
        if (filter.hasOwnProperty(key) && filter[key]) {
          if (key === '_id') {
            query[key] = ObjectId(filter[key])
          } else {
            query[key] = filter[key]
          }
        }
      }
      const cursor = collection.find(query)
      return cursor.toArray()
    }

  })
}
