import eventDbFactory from './event-db-factory'
import { getDbConnection } from '../utils/mongo-connection'

const eventDb = eventDbFactory({ getDbConnection })
// Create userDb instance with dependencies
export {
  eventDb
}
