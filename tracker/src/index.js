import express from 'express'
import {eventHandler} from './use-cases'

const app = express()
app.set('port', process.env.PORT || 3000);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const server = app.listen(app.get('port'),() => {
  console.log('---- tracker service started -----')
  console.log(server.address())
  eventHandler.recordEvents();
})
export default server
