export default (dependencies) => {
  const { getBroker, exchange, queue } = dependencies
  if (!getBroker || !exchange || !queue) {
    throw new Error('dpi failed ')
  }

  return Object.freeze({
    subscribe: async (onMessageReceiced) => {
      const broker = await getBroker(exchange, queue)
      await broker.consume(queue, (message) => {
        if (message.content) {
          console.log(' [x] %s', message.content.toString())
          onMessageReceiced(JSON.parse(message.content))
        }
      }, {
        noAck: true
      })
    }
  })
}
