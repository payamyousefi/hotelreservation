import amqp from 'amqplib'
import brokerFactory from './broker-factory'

const getRabbitMqChannel = async (exchangeName, queueName) => {
  try {
    const connection = await amqp.connect(
        `amqp://${process.env.RABBITMQ_USERNAME}:${
        process.env.RABBITMQ_PASSWORD
        }@rabbitmq`
    )
    const channel = await connection.createChannel()

    await channel.assertExchange(exchangeName, 'fanout', {
      durable: false
    })

    await channel.assertQueue(queueName, {
      exclusive: true
    })

    await channel.bindQueue(
      queueName,
      exchangeName,
      ''
    )
    return channel
  } catch (e) {
    console.log(e)
    process.exit(1)
  }
}

const broker = brokerFactory({ getBroker: getRabbitMqChannel, exchange: 'trivagoEvents', queue: 'trivagoEventsQueue' })

export {
  broker
}
