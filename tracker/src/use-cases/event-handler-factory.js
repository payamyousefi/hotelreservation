
export default (dependencies) => {
  const { broker, eventDb } = dependencies
  if (!broker || !eventDb) {
    throw new Error('dpi failed')
  }

  return Object.freeze({
    recordEvents: async () => {
      await broker.subscribe(async (event) => {
        console.log(`event recevied: ${event}`)
        const result = await eventDb.insert(event)
        console.log(JSON.stringify(result, null, 2))
      })
    }
  })
}
