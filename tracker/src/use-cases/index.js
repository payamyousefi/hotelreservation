import eventHandlerFactory from './event-handler-factory'
import { broker } from '../message-broker'
import { eventDb } from '../data-access'
const eventHandler = eventHandlerFactory({ broker, eventDb })
export {
  eventHandler
}
