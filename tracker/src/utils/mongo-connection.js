import { MongoClient } from 'mongodb'

// Connection URL
// 'mongodb://localhost:27017';
if (!process.env.MONGO_DB_URL) {
  throw new Error('mongo url is empty please set MONGO_DB_URL env')
}
const url = process.env.MONGO_DB_URL

// Database Name
const dbName = process.env.MONGO_DB_NAME || 'trivago'

// Create a new MongoClient
const client = new MongoClient(url)

const getDbConnection = async () => {
  if (!client.isConnected()) {
    // Use connect method to connect to the Server
    await client.connect()
  }
  return client.db(dbName)
}

export {
  getDbConnection
}
