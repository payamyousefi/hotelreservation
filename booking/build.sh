#!/bin/bash -e

REGISTRY="registry.gitlab.com"

build_image(){
    docker build --no-cache -t ${CI_REGISTRY_IMAGE}/booking:latest -f ./booking/Dockerfile.build ./booking
    docker push ${CI_REGISTRY_IMAGE}/booking:latest
}

build_image