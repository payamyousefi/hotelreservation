import controller from '../controller'

import Router from 'express-promise-router'
const router = Router()

router.post('/v1/book', controller.bookRoomCtrl)
router.post('/v1/room', controller.addRoomCtrl)
router.get('/v1/room', controller.searchRoomCtrl)
router.get('/v1/reservelist', controller.searchReservationCtrl)
router.put('/v1/room/availability', controller.changeRoomAvailabilityCtrl)

export default router
