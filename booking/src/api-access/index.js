import getUserFactory from './get-user-factory'
import { get } from 'axios'
const getUser = getUserFactory({
  httpGetApi: get,
  USER_SERVICE_API: process.env.USER_SERVICE_API,
  TOKEN: process.env.SERVICE_TOKEN
})

export {
  getUser
}
