import getUserFactory from './get-user-factory'

describe('test get user api call', () => {
  it('should read user from user-service rest api', async () => {
    const _id = '2837482734'
    const httpGetApi = jest.fn(() => {
      return {
        data: {
          _id,
          name: 'payam',
          family: 'yousefi',
          email: 'root@gmail.com'
        }
      }
    })

    const getUser = getUserFactory({
      httpGetApi: httpGetApi,
      USER_SERVICE_API: 'http://mock_api:3000',
      TOKEN: 'Mock_Token'
    })
    const user = await getUser(_id)
    expect(user._id).toEqual(_id)
    expect(user.name).not.toEqual(undefined)
    expect(user.email).not.toEqual(undefined)
    expect(user.family).not.toEqual(undefined)
  })
})
