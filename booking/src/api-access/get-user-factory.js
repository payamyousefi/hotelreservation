export default dependencies => {
  const { httpGetApi, USER_SERVICE_API, TOKEN } = dependencies
  if (!httpGetApi || !USER_SERVICE_API || !TOKEN) {
    throw new Error('failed to inject dependencies')
  }
  return async (userId) => {
    const response = await httpGetApi(`${USER_SERVICE_API}/user/${userId}`, {
      headers: {
        Authorization: TOKEN
      }
    })
    return response.data
  }
}
