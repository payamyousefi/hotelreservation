import express from 'express'
import routes from './routes'

const app = express()

app.set('port', process.env.PORT || 3000);
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use('/', routes);

const server = app.listen(app.get('port'),() => {
  console.log('---- booking service started -----')
  console.log(server.address())
})

export default server
