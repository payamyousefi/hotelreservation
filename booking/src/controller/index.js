import { bookRoom, addRoom, searchRoom, searchReservation, changeRoomAvailability } from '../use-cases'
import createError from 'http-errors'

const changeRoomAvailabilityCtrl = async (request, response) => {
  const { roomId, amount } = request.body
  if (!roomId) {
    throw createError.BadRequest('roomId is required')
  }
  if (amount === undefined) {
    throw createError.BadRequest('amount is required')
  }
  const result = await changeRoomAvailability(roomId, amount)
  response.send(result)
}

const bookRoomCtrl = async (request, response) => {
  const { userId, roomId } = request.body
  if (!userId) {
    throw createError.BadRequest('userId is required')
  }
  if (!roomId) {
    throw createError.BadRequest('roomId is required')
  }
  const result = await bookRoom({ userId, roomId })
  response.send(result)
}

const searchRoomCtrl = async (request, response) => {
  const filter = request.query
  const rooms = await searchRoom(filter)
  response.send(rooms)
}

const searchReservationCtrl = async (request, response) => {
  const filter = request.query
  const reservations = await searchReservation(filter)
  response.send(reservations)
}

const addRoomCtrl = async (request, response) => {
  const { name, requiredPoints, availableAmount } = request.body
  if (!availableAmount) {
    throw createError.BadRequest('availableAmount is required')
  }
  if (!requiredPoints) {
    throw createError.BadRequest('requiredPoints is required')
  }
  if (!name) {
    throw createError.BadRequest('name is required')
  }

  const result = await addRoom({ availableAmount, requiredPoints, name })
  response.send(result)
}

export default {
  changeRoomAvailabilityCtrl,
  addRoomCtrl,
  bookRoomCtrl,
  searchReservationCtrl,
  searchRoomCtrl
}
