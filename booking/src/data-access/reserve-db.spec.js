import { createMockDb, closeMockDbConnection } from './db-connection-mock'
import reserveDbFactory from './reserve-db-factory'
import { ObjectId } from 'mongodb'
describe('insert new reserve', () => {
  let reserveDb
  beforeAll(async () => {
    reserveDb = reserveDbFactory({ getDbConnection: createMockDb })
  })

  afterAll(async () => {
    await closeMockDbConnection()
  })

  it('should insert a doc into mongo collection', async () => {
    const mockReserve = { _id: new ObjectId(), userId: '12123' }
    const createdReserve = await reserveDb.insert(mockReserve)
    expect(createdReserve._id).toEqual(mockReserve._id)
    expect(createdReserve).toEqual(mockReserve)
  })

  it('should remove a doc from mongo collection', async () => {
    const mockReserve = { _id: new ObjectId() }
    await reserveDb.insert(mockReserve)
    const result = await reserveDb.delete(mockReserve._id)

    expect(result.ok).toBe(true)
    expect(result.removed).toBe(1)
  })

  it('should update a doc in mongo collection', async () => {
    const mockReserve = { _id: new ObjectId(), roomId: '2384723', userId: '1232' }
    await reserveDb.insert(mockReserve)
    const updatedReserve = await reserveDb.update(mockReserve._id, { userId: '83746' })
    expect(updatedReserve.name).toEqual(mockReserve.name)
    expect(updatedReserve.userId).toEqual('83746')
  })

  it('should find room by id', async () => {
    const mockRoom = { _id: new ObjectId(), name: 'Economy Fresh' }
    await reserveDb.insert(mockRoom)
    const selectedRoom = await reserveDb.findById(mockRoom._id)
    expect(selectedRoom._id).toEqual(mockRoom._id)
  })
})
