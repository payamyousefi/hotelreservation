import { ObjectId } from 'mongodb'

export default (dependencies) => {
  const { getDbConnection } = dependencies
  if (!getDbConnection) {
    throw new Error('dependency injection failed')
  }
  const COLLECTION_NAME = 'rooms'
  return Object.freeze({

    // insert a room-data into mongodb
    insert: async (roomData) => {
      const db = await getDbConnection()
      const roomCollection = db.collection(COLLECTION_NAME)
      const result = await roomCollection.insertOne(roomData)
      return roomCollection.findOne({ _id: ObjectId(result.insertedId) })
    },

    // find room from mongodb by id
    findById: async (_id) => {
      const db = await getDbConnection()
      const roomCollection = db.collection(COLLECTION_NAME)
      return roomCollection.findOne({ _id: ObjectId(_id) })
    },

    // delete room from mongodb
    delete: async (_id) => {
      const db = await getDbConnection()
      const roomCollection = db.collection(COLLECTION_NAME)
      const response = await roomCollection.deleteOne({ _id: ObjectId(_id) }, { justOne: true })
      // return number of removed objects
      return { removed: response.result.n, ok: response.result.ok === 1 }
    },

    // find room by filter
    find: async (filter = {}) => {
      const db = await getDbConnection()
      const collection = db.collection(COLLECTION_NAME)
      const query = {}
      for (const key in filter) {
        /* eslint-disable-next-line */
        if (filter.hasOwnProperty(key) && filter[key]) {
          if (key === '_id') {
            query[key] = ObjectId(filter[key])
          } else {
            query[key] = filter[key]
          }
        }
      }
      const cursor = collection.find(query)
      return cursor.toArray()
    },

    // update room attributes
    update: async (_id, updates) => {
      const db = await getDbConnection()
      const roomCollection = db.collection(COLLECTION_NAME)
      await roomCollection.updateOne({ _id: ObjectId(_id) }, { $set: { ...updates } })
      return roomCollection.findOne({ _id: ObjectId(_id) })
    }

  })
}
