import { ObjectId } from 'mongodb'
export default (dependencies) => {
  const { getDbConnection } = dependencies
  if (!getDbConnection) {
    throw new Error('dependency injection failed')
  }

  const COLLECTION_NAME = 'reserves'

  return Object.freeze({

    // insert a reserve-data into mongodb
    insert: async (reserveData) => {
      const db = await getDbConnection()
      const reserveCollection = db.collection(COLLECTION_NAME)
      const result = await reserveCollection.insertOne(reserveData)
      return reserveCollection.findOne({ _id: ObjectId(result.insertedId) })
    },

    // find reserve from mongodb by id
    findById: async (_id) => {
      const db = await getDbConnection()
      const reserveCollection = db.collection(COLLECTION_NAME)
      return reserveCollection.findOne({ _id: ObjectId(_id) })
    },

    // search reserve by filter
    find: async (filter = {}) => {
      const db = await getDbConnection()
      const collection = db.collection(COLLECTION_NAME)
      const query = {}
      for (const key in filter) {
        /* eslint-disable-next-line */
        if (filter.hasOwnProperty(key) && filter[key]) {
          if (key === '_id') {
            query[key] = ObjectId(filter[key])
          } else {
            query[key] = filter[key]
          }
        }
      }
      const cursor = collection.find(query)
      return cursor.toArray()
    },

    // delete reserve from mongodb
    delete: async (_id) => {
      const db = await getDbConnection()
      const reserveCollection = db.collection(COLLECTION_NAME)
      const response = await reserveCollection.deleteOne({ _id: ObjectId(_id) }, { justOne: true })
      // return number of removed objects
      return { removed: response.result.n, ok: response.result.ok === 1 }
    },

    // update reserve attributes
    update: async (_id, updates) => {
      const db = await getDbConnection()
      const reserveCollection = db.collection(COLLECTION_NAME)
      await reserveCollection.updateOne({ _id: ObjectId(_id) }, { $set: { ...updates } })
      return reserveCollection.findOne({ _id: ObjectId(_id) })
    }

  })
}
