import { ObjectId } from 'mongodb'
import { createMockDb, closeMockDbConnection } from './db-connection-mock'
import roomDbFactory from './room-db-factory'

describe('insert new room', () => {
  let roomDb
  beforeAll(async () => {
    roomDb = roomDbFactory({ getDbConnection: createMockDb })
  })

  afterAll(async () => {
    await closeMockDbConnection()
  })

  it('should insert a doc into mongo collection', async () => {
    const mockRoom = { _id: new ObjectId(), name: 'Economy Room' }
    const createdRoom = await roomDb.insert(mockRoom)
    expect(createdRoom._id).toEqual(mockRoom._id)
    expect(createdRoom).toEqual(mockRoom)
  })

  it('should remove a doc from mongo collection', async () => {
    const mockRoom = { _id: new ObjectId(), name: 'Economy 1' }
    await roomDb.insert(mockRoom)
    const result = await roomDb.delete(mockRoom._id)

    expect(result.ok).toBe(true)
    expect(result.removed).toBe(1)
  })

  it('should update a doc in mongo collection', async () => {
    const mockRoom = { _id: new ObjectId(), name: 'Economy 2', availableAmount: 2 }
    await roomDb.insert(mockRoom)
    const updatedRoom = await roomDb.update(mockRoom._id, { availableAmount: 20 })
    expect(updatedRoom.name).toEqual(mockRoom.name)
    expect(updatedRoom.availableAmount).toEqual(20)
  })

  it('should find room by id', async () => {
    const mockRoom = { _id: new ObjectId(), name: 'Economy Fresh' }
    await roomDb.insert(mockRoom)
    const selectedRoom = await roomDb.findById(mockRoom._id)
    expect(selectedRoom._id).toEqual(mockRoom._id)
  })
})
