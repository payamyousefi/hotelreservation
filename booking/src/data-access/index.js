import roomDbFactory from './room-db-factory'
import reserveDbFactory from './reserve-db-factory'
import { getDbConnection } from '../utils/mongo-connection'
// Create userDb instance with dependencies
const roomDb = roomDbFactory({ getDbConnection })
const reserveDb = reserveDbFactory({ getDbConnection })

export {
  reserveDb,
  roomDb
}
