import { MongoClient } from 'mongodb'

let connection
let db

// create a reusable mock instance for mongodb
const createMockDb = async () => {
  connection = await MongoClient.connect(global.__MONGO_URI__, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  return connection.db(global.__MONGO_DB_NAME__)
}

// close mock instance connection after doing tests
const closeMockDbConnection = async () => {
  if (connection) {
    await connection.close()
  }

  if (db) {
    await db.close()
  }
}

export {
  createMockDb,
  closeMockDbConnection
}
