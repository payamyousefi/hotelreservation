export default (dependencies) => {
  const { getBroker, exchange } = dependencies
  if (!getBroker || !exchange) {
    throw new Error('dpi failed ')
  }

  return Object.freeze({
    publish: async (message) => {
      const broker = await getBroker(exchange)
      await broker.publish(exchange, '', Buffer.from(JSON.stringify(message)))
    }
  })
}
