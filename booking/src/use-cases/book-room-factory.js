import room from '../entities/room'
import reserve from '../entities/reserve'
import { createEvent } from '../entities/event'

export default dependencies => {
  const { getUser, roomDb, reserveDb, broker } = dependencies
  if (!roomDb || !reserveDb || !broker) {
    throw new Error('dependency injection failed')
  }
  return async reserveInfo => {
    const { userId, roomId } = reserveInfo
    // load user info from api
    const user = await getUser(userId)
    console.log({ user })
    const roomInfo = await roomDb.findById(roomId)
    const roomObj = room(roomInfo)

    // Create an empty reserve object
    const reserveObj = reserve()
    reserveObj.book({ roomId: roomId, userId: userId })
    if (user.points > roomObj.getRequiredPoints()) {
      // set reserved status
      reserveObj.setReservedStatus()
    } else {
      // set pending status
      reserveObj.setPendingStatus()
    }

    const reserved = await reserveDb.insert({
      roomId: reserveObj.getRoomId(),
      userId: reserveObj.getUserId(),
      reserveDate: reserveObj.getReserveDate(),
      status: reserveObj.getStatus()
    })
    const event = createEvent({ verb: 'room-reserved', meta: reserved })
    await broker.publish(event)

    return reserved
  }
}
