import addRoomFactory from './add-room-factory'
import roomDbFactory from '../data-access/room-db-factory'
import { createMockDb } from '../data-access/db-connection-mock'
describe('test add room use-case', () => {
  let roomDb
  beforeAll(() => {
    roomDb = roomDbFactory({ getDbConnection: createMockDb })
  })
  it('should add new room', async () => {
    const roomInfo = {
      availableAmount: 100,
      requiredPoints: 20,
      name: 'Economy'
    }

    const addRoom = addRoomFactory({ roomDb })
    const room = await addRoom(roomInfo)
    expect(room._id).not.toEqual(undefined)
    expect(room.availableAmount).toEqual(roomInfo.availableAmount)
    expect(room.requiredPoints).toEqual(roomInfo.requiredPoints)
    expect(room.name).toEqual(roomInfo.name)
  })
})
