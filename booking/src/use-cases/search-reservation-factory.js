export default (dependencies) => {
  const { reserveDb } = dependencies
  if (!reserveDb) {
    throw new Error('dependency injection failed')
  }
  return async filter => {
    return reserveDb.find(filter)
  }
}
