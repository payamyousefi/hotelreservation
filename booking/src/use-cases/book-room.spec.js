import bookRoomFactory from './book-room-factory'
import { createMockDb, closeMockDbConnection } from '../data-access/db-connection-mock'
import reserveDbFactory from '../data-access/reserve-db-factory'
import roomDbFactory from '../data-access/room-db-factory'
import { ObjectId } from 'mongodb'

describe('test reserving a room for for user', () => {
  let reserveDb
  let roomDb
  beforeAll(async () => {
    reserveDb = reserveDbFactory({ getDbConnection: createMockDb })
    roomDb = roomDbFactory({ getDbConnection: createMockDb })
  })

  afterAll(async () => {
    await closeMockDbConnection()
  })

  it('should reserve a room for user', async () => {
    const room = await roomDb.insert({
      _id: new ObjectId(),
      name: 'economy class',
      requiredPoints: 20,
      availableAmount: 2
    })
    const mockUser = {
      _id: new ObjectId(),
      points: 100
    }
    const getUser = jest.fn(() => {
      return Promise.resolve(mockUser)
    })

    const bookRoom = bookRoomFactory({
      getUser,
      roomDb,
      reserveDb,
      broker: {
        publish: jest.fn(() => {})
      }
    })
    const reserve = await bookRoom({ roomId: room._id, userId: mockUser._id })
    expect(reserve).not.toEqual(undefined)
    expect(reserve.userId).toEqual(mockUser._id)
    expect(reserve.roomId).toEqual(room._id)
  })
})
