import bookRoomFactory from './book-room-factory'
import searchReservationFactory from './search-reservation-factory'
import changeRoomAvailabilityFactory from './change-room-availability'
import addRoomFactory from './add-room-factory'
import searchRoomFactory from './search-room-factory'
import { roomDb, reserveDb } from '../data-access'
import { getUser } from '../api-access'
import { broker } from '../message-broker'

const bookRoom = bookRoomFactory({ getUser, roomDb, reserveDb, broker })
const addRoom = addRoomFactory({ roomDb })
const searchRoom = searchRoomFactory({ roomDb })
const searchReservation = searchReservationFactory({ reserveDb })
const changeRoomAvailability = changeRoomAvailabilityFactory({ roomDb, broker })

export {
  changeRoomAvailability,
  searchRoom,
  searchReservation,
  bookRoom,
  addRoom
}
