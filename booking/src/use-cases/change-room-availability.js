import room from '../entities/room'
import { createEvent } from '../entities/event'

export default dependencies => {
  const { roomDb, broker } = dependencies
  if (!roomDb) {
    throw new Error('dependency injection failed')
  }

  return async (roomId, amount) => {
    console.log({ roomId })
    console.log({ amount })
    const roomInfo = await roomDb.findById(roomId)

    const roomObj = room(roomInfo)
    roomObj.addAvailableRoom(amount)

    const updateResult = await roomDb.update(roomId, { availableAmount: roomObj.getAvailableAmount() })

    const event = createEvent({ verb: 'room-availability-changed', meta: updateResult })
    await broker.publish(event)

    return updateResult
  }
}
