export default (dependencies) => {
  const { roomDb } = dependencies
  if (!roomDb) {
    throw new Error('dependency injection failed')
  }
  return async filter => {
    return roomDb.find(filter)
  }
}
