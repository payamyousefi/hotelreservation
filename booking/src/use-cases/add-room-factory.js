import room from '../entities/room'

export default dependencies => {
  const { roomDb } = dependencies
  if (!roomDb) {
    throw new Error('dependency injection failed')
  }

  return async roomInfo => {
    const roomObj = room(roomInfo)
    return roomDb.insert({
      name: roomObj.getName(),
      availableAmount: roomObj.getAvailableAmount(),
      requiredPoints: roomObj.getRequiredPoints()
    })
  }
}
