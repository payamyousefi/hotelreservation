export default () => {
  
  return (messageInfo) => {
    const {verb,meta}=  messageInfo
    if(!verb || !meta){
      throw new Error('invalid message info');
    }
    const producer="booking-service"
    
    return {
      producer,
      verb,
      meta,
      creationDate:new Date()
    }
  }
}
