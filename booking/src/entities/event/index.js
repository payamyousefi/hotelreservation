import createEventFactory from './create-event-factory'

const createEvent = createEventFactory()

export {
  createEvent
}
