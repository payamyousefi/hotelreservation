
export default dependencies => {
  return () => {
    let reserveDate, roomId, userId, status

    return Object.freeze({
      getRoomId: () => roomId,
      getUserId: () => userId,
      setReservedStatus: () => (status = 'RESERVED'),
      setPendingStatus: () => (status = 'PENDING_APPROVAL'),
      getStatus: () => status,
      getReserveDate: () => reserveDate,
      book: (reserve) => {
        roomId = reserve.roomId
        userId = reserve.userId
        reserveDate = new Date()
      }
    })
  }
}
