import reserve from '.'

describe('test book object', () => {
  it('should create empty reserve with date', () => {
    const reserveObj = reserve()
    const roomId = '2873647834'
    const userId = '8348858457'
    reserveObj.book({ roomId, userId })
    expect(reserveObj.getReserveDate()).not.toEqual(undefined)
    expect(reserveObj.getRoomId()).toEqual(roomId)
    expect(reserveObj.getUserId()).toEqual(userId)
  })

  it('should set reserve status to reserved', () => {
    const reserveObj = reserve()
    const roomId = '2873647834'
    const userId = '8348858457'
    reserveObj.book({ roomId, userId })
    reserveObj.setReservedStatus()
    expect(reserveObj.getStatus()).toBe('RESERVED')
  })

  it('should set reserve status to pending aproval', () => {
    const reserveObj = reserve()
    const roomId = '2873647834'
    const userId = '8348858457'
    reserveObj.book({ roomId, userId })
    reserveObj.setPendingStatus()
    expect(reserveObj.getStatus()).toBe('PENDING_APPROVAL')
  })
})
