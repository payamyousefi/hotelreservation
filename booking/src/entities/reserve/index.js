import reserveFactory from './reserve-factory'

const reserve = reserveFactory()

export default reserve
