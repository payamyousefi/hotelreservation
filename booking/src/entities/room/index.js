import roomFactory from './room-factory'

const room = roomFactory({})

export default room
