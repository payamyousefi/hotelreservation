import room from '../room'

describe('test room entity', () => {
  it('should throw error on empty room info ', () => {
    expect(() => room()).toThrowError('room info is required')
  })

  it('should throw error on empty name', () => {
    expect(() => room({})).toThrowError('room name is required')
  })

  it('should throw error on empty availableAmount', () => {
    expect(() => room({
      name: 'Economy Single Room'
    })).toThrowError('availableAmount is required')
  })

  it('should throw error on empty required_points', () => {
    expect(() => room({
      name: 'Economy Single Room',
      availableAmount: 100
    })).toThrowError('requiredPoints is required')
  })

  it('should create a rome', () => {
    const roomInfo = {
      name: 'Economy Single Room',
      availableAmount: 100,
      requiredPoints: 25
    }
    const roomObj = room(roomInfo)
    expect(roomObj.getName()).toEqual(roomInfo.name)
    expect(roomObj.getAvailableAmount()).toEqual(roomInfo.availableAmount)
    expect(roomObj.getRequiredPoints()).toEqual(roomInfo.requiredPoints)
  })

  it('should increase number of available room', () => {
    const roomInfo = {
      name: 'Economy Single Room',
      availableAmount: 100,
      requiredPoints: 25
    }
    const roomObj = room(roomInfo)
    roomObj.addAvailableRoom(10)
    expect(roomObj.getAvailableAmount()).toBe(110)
  })
})
