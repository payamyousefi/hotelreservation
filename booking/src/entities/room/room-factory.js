
export default dependencies => {
  return roomInfo => {
    if (!roomInfo) {
      throw new Error('room info is required')
    }
    const { name, requiredPoints } = roomInfo
    let { availableAmount = 0 } = roomInfo
    if (!name) {
      throw new Error('room name is required')
    }

    if (!availableAmount) {
      throw new Error('availableAmount is required')
    }

    if (!requiredPoints) {
      throw new Error('requiredPoints is required')
    }

    return Object.freeze({
      getName: () => name,
      getAvailableAmount: () => availableAmount,
      getRequiredPoints: () => requiredPoints,
      addAvailableRoom: (moreRoomCapacity) => (availableAmount += moreRoomCapacity)
    })
  }
}
