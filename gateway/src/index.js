import express from 'express'
import { ApolloServer, AuthenticationError } from 'apollo-server-express';
import { appModule } from './modules';
import cors from 'cors'
import jwt from 'jsonwebtoken'
import tokenGen from './issueToken'
const JWT_SECRET = process.env.JWT_SECRET
if(!JWT_SECRET){
  throw new Error("JWT_SECRET is empty, please add it to env")
}
const app = express();
const apolloServer = new ApolloServer({
    schema: appModule,
    context: ({ req, res }) => {
      let token = req.headers.authorization || '';
      console.log(token);
      token = token.replace('Bearer ', '');
      let user;
      try {
          user = jwt.verify(token,JWT_SECRET);
      } catch (error) {
          console.log(error);
          throw new AuthenticationError('you must add a valid token in request Authorization header, please check the README');
      }
      return user;
    }
  });

app.set('port', process.env.PORT || '3000');
app.set('host', process.env.HOST || '0.0.0.0');
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
apolloServer.applyMiddleware({ app, path: '/api' });
  
const server = app.listen(app.get('port'), app.get('host'), () => {
    console.log(`🚀 Gateway ready at ${JSON.stringify(server.address())}`);
  });