import gql from 'graphql-tag';

export default gql`
  type User {
    _id: ID
    name: String
    family: String
    email: String
    username: String
    points:Int
  }

  type Query {
    users(name:String,family:String,email:String): [User] @auth(role:"private")
    user(_id: ID): User @auth(role:"private")
  }
  
  type Mutation {
    createUser(name: String, family: String,email:String): User @auth(role:"private")
    updateBonusPoints(userId: String, points: Int): User @auth(role:"private")
  }
`;
