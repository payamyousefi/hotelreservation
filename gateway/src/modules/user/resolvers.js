import httpClientFactory from '../../utils/http-client';

const httpClient = httpClientFactory(process.env.USER_API);
export default {
  Query: {
    users:  async(root,args,context) => {
      const response = await httpClient.get(`/v1/user`,{
        params:{
          ...args
        }
      });
      return response.data;
    },
    user: async(root,args,context) => {
      const response = await httpClient.get(`/v1/user/${args._id}`);
      return response.data;
    },
  },
  Mutation: {
    updateBonusPoints: async (root,args,context)=>{
      const {userId,points} = args;
      const response = await httpClient.put('/v1/user/point',{
        userId,
        points
      })
      return response.data
    },
    createUser: async ( root,args,context ) => {
      const { _id,name,family,email} = args;
      const response = await httpClient.post(`/v1/user`,{
        _id,
        name,
        family,
        email
      });
      return response.data;
      }
    }
  }

