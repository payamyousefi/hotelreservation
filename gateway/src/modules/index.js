import { userResolvers, userSchemas } from './user'
import { reserveSchema, reserveResolvers } from './reserve'
import { roomResolvers, roomSchemas } from './room'
import { authSchemas, AuthDirective } from './auth'
import { makeExecutableSchema } from 'graphql-tools'

export const appModule = makeExecutableSchema({
  typeDefs: [
    authSchemas,
    userSchemas,
    reserveSchema,
    roomSchemas
  ],
  resolvers: [
    userResolvers,
    reserveResolvers,
    roomResolvers
  ],
  schemaDirectives: {
    auth: AuthDirective
  }
})
