import gql from 'graphql-tag';

export default gql`
  type Reserve {
    _id: ID
    roomId: String
    userId: String
    reserveDate: String
    status: String
  }

  extend type Query {
    reserveList(status:String): [Reserve] @auth(role:"private")
  }
  
  extend type Mutation {
    book(userId: String, roomId: String): Reserve @auth(role:"public")
  }
`;
