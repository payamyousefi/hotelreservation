import reserveSchema from './schema';
import reserveResolvers from './resolvers';

export { reserveResolvers , reserveSchema  };
