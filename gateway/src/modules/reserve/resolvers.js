import httpClientFactory from '../../utils/http-client';

const httpClient = httpClientFactory(process.env.BOOKING_API);
export default {
  Query: {
    reserveList:  async(root,args,context) => {
      const response = await httpClient.get(`/v1/reservelist`,{
        params:{
          ...args
        }
      });
      return response.data;
    },
  },
  Mutation: {
    book: async (root,args,context)=>{
      const {userId,roomId} = args;
      const response = await httpClient.post('/v1/book',{
        userId,
        roomId
      })
      return response.data
    }
  }
}

