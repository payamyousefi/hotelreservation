import {SchemaDirectiveVisitor} from 'graphql-tools';
import { ForbiddenError } from 'apollo-server-errors';

export class AuthDirective extends SchemaDirectiveVisitor {

    visitFieldDefinition(field,details){
        
        const {resolve} = field;
        const {role} = this.args;
        
        if (!role) {
            throw new Error('public api are not allowed');
        }

        field.resolve = async (...args) => {
            console.log('----- ## Role Based Access Control Check ## ------');
            const apiContext = args[2];
            if(role === apiContext.role){
                const result = await resolve.apply(this, args);
                return result;
            }else{
                throw new ForbiddenError(`You dont have permission to call this api `);
            }
        };
    }
}

