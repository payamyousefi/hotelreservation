import gql from 'graphql-tag';

export default gql`
    
    directive @auth(
        role: String
    ) on FIELD_DEFINITION
 
`;
