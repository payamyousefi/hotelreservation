import roomSchemas from './schema';
import roomResolvers from './resolvers';

export { roomResolvers , roomSchemas  };
