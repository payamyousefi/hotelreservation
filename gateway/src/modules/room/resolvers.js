import httpClientFactory from '../../utils/http-client';

const httpClient = httpClientFactory(process.env.BOOKING_API);
export default {
  Query: {
    rooms:  async(root,args,context) => {
      const response = await httpClient.get(`/v1/room`,{
        params:{
          ...args
        }
      });
      return response.data;
    }
  },
  Mutation: {
    updateAvailableRoom: async (root,args,context)=>{
      const {roomId,amount} = args;
      const response = await httpClient.put('/v1/room/availability',{
        amount,
        roomId
      })
      return response.data;
    }
  }
}
