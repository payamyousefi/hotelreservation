import gql from 'graphql-tag';


export default gql`
  type Room {
    _id: ID
    name: String
    availableAmount: Int
    requiredPoints: Int
  }

  extend type Query {
    rooms(_id:ID, name:String): [Room] @auth(role:"private")
  }
  
  extend type Mutation {
    updateAvailableRoom(roomId: String, amount:Int): Room @auth(role:"private")
  }
`
