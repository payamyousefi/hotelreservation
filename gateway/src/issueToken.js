const TOKEN_SECRET=process.env.JWT_SECRET
var jwt = require('jsonwebtoken');
var publicApiToken = jwt.sign({ role: 'public',  exp: Math.floor(Date.now() / 1000) + (60 * 60) * 36000,
}, TOKEN_SECRET);

var privateApiToken = jwt.sign({ role: 'private',  exp: Math.floor(Date.now() / 1000) + (60 * 60) * 36000,
}, TOKEN_SECRET);
console.log (' ############# YOUR TOKENS TO CALL GQL API, COPY THEM CAREFULLY #############')
console.log(`PUBLIC API Token: ${publicApiToken}`)
console.log(`PRIVATE API Token: ${privateApiToken}`)

console.log (' ############# END #############')