import axios from 'axios'
const HTTP_TIME_OUT = 60000

export default (apiBaseUrl) => {
  const axInstance = axios.create({
    baseURL: apiBaseUrl,
    timeout: HTTP_TIME_OUT
  })

  axInstance.interceptors.request.use(
    (config) => {
      return config
    },
    (error) => {
      // Do something with request error
      return Promise.reject(error)
    }
  )

  axInstance.interceptors.response.use(
    (response) => {
      response.headers = response.headers || {}
      response.headers.isSuccess =
        response.status >= 200 && response.status < 300
      return response
    },
    (error) => {
      try {
        const { config, status, statusText } = error.response
        console.log(config.method)
        console.log(config.url)
        console.log(`${status}:${statusText}`)
      } catch (e) {
        //
      }

      // Do something with response error
      return Promise.reject(error)
    }
  )
  return axInstance
}
