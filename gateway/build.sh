#!/bin/bash -e

REGISTRY="registry.gitlab.com"

build_image(){
    docker build --no-cache -t ${CI_REGISTRY_IMAGE}/gateway:latest -f ./gateway/Dockerfile.build ./gateway
    docker push ${CI_REGISTRY_IMAGE}/gateway:latest
}

build_image